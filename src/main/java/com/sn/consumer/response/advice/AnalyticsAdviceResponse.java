package com.sn.consumer.response.advice;

import com.sn.consumer.entity.advice.AnalyticsAdvice;
import com.sn.consumer.response.base.AbstractGenericResponse;

import javax.validation.constraints.NotNull;

public class AnalyticsAdviceResponse extends AbstractGenericResponse {

    private String id;

    @NotNull
    private String name;

    @NotNull
    private String description;

    @NotNull
    private String variable;

    @NotNull
    private String advice;

    @NotNull
    private String thumbnailUrl;

    @NotNull
    private String language;

    @NotNull
    private Integer referenceId;

    @NotNull
    private Boolean active;

    @NotNull
    private String createdBy;

    @NotNull
    private Integer priority;


    private String patternId;

    private String patternPattern;

    private String servicesId;

    private String servicesName;

    public AnalyticsAdviceResponse(AnalyticsAdvice analyticsAdvice) {
        this.id = analyticsAdvice.getId();
        this.name = analyticsAdvice.getName();
        this.description = analyticsAdvice.getDescription();
        this.variable = analyticsAdvice.getVariable();
        this.advice = analyticsAdvice.getAdvice();
        this.thumbnailUrl = analyticsAdvice.getThumbnailUrl();
        this.language = analyticsAdvice.getLanguage();
        this.referenceId = analyticsAdvice.getReferenceId();
        this.active = analyticsAdvice.isActive();
        this.createdBy = analyticsAdvice.getCreatedBy();
        this.priority = analyticsAdvice.getPriority();
        this.patternId = analyticsAdvice.getPattern().getId();
        this.patternPattern = analyticsAdvice.getPattern().getPattern();
        this.servicesId = analyticsAdvice.getServices().getId();
        this.servicesName = analyticsAdvice.getServices().getName();
    }

    public AnalyticsAdviceResponse() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getVariable() {
        return variable;
    }

    public void setVariable(String variable) {
        this.variable = variable;
    }

    public String getAdvice() {
        return advice;
    }

    public void setAdvice(String advice) {
        this.advice = advice;
    }

    public String getThumbnailUrl() {
        return thumbnailUrl;
    }

    public void setThumbnailUrl(String thumbnailUrl) {
        this.thumbnailUrl = thumbnailUrl;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public Integer getReferenceId() {
        return referenceId;
    }

    public void setReferenceId(Integer referenceId) {
        this.referenceId = referenceId;
    }

    public Boolean isActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public String getPatternId() {
        return patternId;
    }

    public void setPatternId(String analyticsPatternId) {
        this.patternId = analyticsPatternId;
    }

    public String getPatternPattern() {
        return patternPattern;
    }

    public void setPatternPattern(String analyticsPatternPattern) {
        this.patternPattern = analyticsPatternPattern;
    }

    public String getServicesId() {
        return servicesId;
    }

    public void setServicesId(String servicesId) {
        this.servicesId = servicesId;
    }

    public String getServicesName() {
        return servicesName;
    }

    public void setServicesName(String servicesName) {
        this.servicesName = servicesName;
    }

}
