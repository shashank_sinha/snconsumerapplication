package com.sn.consumer.response.advice;

import com.sn.consumer.entity.advice.AnalyticsPattern;
import com.sn.consumer.response.base.AbstractGenericResponse;


import javax.validation.constraints.NotNull;

public class AnalyticsPatternResponse extends AbstractGenericResponse {

    private String id;

    @NotNull
    private String pattern;

    @NotNull
    private Integer referenceId;

    @NotNull
    private String createdBy;

    @NotNull
    private String language;

    @NotNull
    private Boolean active;

    public AnalyticsPatternResponse(AnalyticsPattern analyticsPattern) {
        this.id = analyticsPattern.getId();
        this.pattern = analyticsPattern.getPattern();
        this.referenceId = analyticsPattern.getReferenceId();
        this.createdBy = analyticsPattern.getCreatedBy();
        this.language = analyticsPattern.getLanguage();
        this.active = analyticsPattern.isActive();
    }

    public AnalyticsPatternResponse() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPattern() {
        return pattern;
    }

    public void setPattern(String pattern) {
        this.pattern = pattern;
    }

    public Integer getReferenceId() {
        return referenceId;
    }

    public void setReferenceId(Integer referenceId) {
        this.referenceId = referenceId;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public Boolean isActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

}
