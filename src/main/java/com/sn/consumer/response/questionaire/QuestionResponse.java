package com.sn.consumer.response.questionaire;


import com.sn.consumer.entity.questionaire.Questionaire;
import org.apache.commons.lang3.StringUtils;


import java.util.List;

public class QuestionResponse {

    private String questionId;
    private String text;
    private String questionType;
    private int priority;
    private String parentQuestionId;
    private String parentQuestionText;
    private String childQuestionId;
    private String childQuestionText;
    private List<String> options;
    private int rating;
    private boolean demonstrated;
    private boolean leaderQuestion;
    private boolean jobRoleQuestion;
    private boolean organisationQuestion;
    private int numberOfTextFields;
    private int numberOfStaticFields;
    private int numberOfLookupFields;
    private String defaultAnswer;
    private boolean trigger;

    private List<QuestionResponse> childQuestionList;

    public QuestionResponse(Questionaire questionaire) {
        this.questionId = questionaire.getId();
        this.text = questionaire.getText();
        this.questionType = questionaire.getQuestiontype().getName();
        this.priority = questionaire.getPriority();

        if(StringUtils.isNotBlank(questionaire.getRating())) {
            this.rating = Integer.parseInt(questionaire.getRating());
        }
        this.demonstrated = questionaire.getDemonstrated();

        this.leaderQuestion = questionaire.getLeaderQuestion() != null ? questionaire.getLeaderQuestion() : false;
        this.jobRoleQuestion = questionaire.getJobRoleQuestion()  != null ? questionaire.getJobRoleQuestion() : false;
        this.organisationQuestion = questionaire.getOrganisationQuestion() != null ? questionaire.getOrganisationQuestion() : false;

        this.numberOfTextFields = questionaire.getNumberOfTextFields() != null ? questionaire.getNumberOfTextFields() : 0;
        this.numberOfLookupFields = questionaire.getNumberOfLookupFields() != null  ? questionaire.getNumberOfLookupFields() : 0;
        this.numberOfStaticFields = questionaire.getNumberOfStaticFields() != null  ? questionaire.getNumberOfStaticFields() : 0;
    }

    public QuestionResponse() {

    }

    public List<QuestionResponse> getChildQuestionList() {
        return childQuestionList;
    }

    public void setChildQuestionList(List<QuestionResponse> childQuestionList) {
        this.childQuestionList = childQuestionList;
    }

    public String getDefaultAnswer() {
        return defaultAnswer;
    }

    public void setDefaultAnswer(String defaultAnswer) {
        this.defaultAnswer = defaultAnswer;
    }

    public boolean isLeaderQuestion() {
        return leaderQuestion;
    }

    public void setLeaderQuestion(boolean leaderQuestion) {
        this.leaderQuestion = leaderQuestion;
    }

    public boolean isJobRoleQuestion() {
        return jobRoleQuestion;
    }

    public void setJobRoleQuestion(boolean jobRoleQuestion) {
        this.jobRoleQuestion = jobRoleQuestion;
    }

    public boolean isOrganisationQuestion() {
        return organisationQuestion;
    }

    public void setOrganisationQuestion(boolean organisationQuestion) {
        this.organisationQuestion = organisationQuestion;
    }

    public int getNumberOfTextFields() {
        return numberOfTextFields;
    }

    public void setNumberOfTextFields(int numberOfTextFields) {
        this.numberOfTextFields = numberOfTextFields;
    }

    public int getNumberOfStaticFields() {
        return numberOfStaticFields;
    }

    public void setNumberOfStaticFields(int numberOfStaticFields) {
        this.numberOfStaticFields = numberOfStaticFields;
    }

    public int getNumberOfLookupFields() {
        return numberOfLookupFields;
    }

    public void setNumberOfLookupFields(int numberOfLookupFields) {
        this.numberOfLookupFields = numberOfLookupFields;
    }

    public String getQuestionId() {
        return questionId;
    }

    public void setQuestionId(String questionId) {
        this.questionId = questionId;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getQuestionType() {
        return questionType;
    }

    public void setQuestionType(String questionType) {
        this.questionType = questionType;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public String getParentQuestionId() {
        return parentQuestionId;
    }

    public void setParentQuestionId(String parentQuestionId) {
        this.parentQuestionId = parentQuestionId;
    }

    public String getParentQuestionText() {
        return parentQuestionText;
    }

    public void setParentQuestionText(String parentQuestionText) {
        this.parentQuestionText = parentQuestionText;
    }

    public String getChildQuestionId() {
        return childQuestionId;
    }

    public void setChildQuestionId(String childQuestionId) {
        this.childQuestionId = childQuestionId;
    }

    public String getChildQuestionText() {
        return childQuestionText;
    }

    public void setChildQuestionText(String childQuestionText) {
        this.childQuestionText = childQuestionText;
    }

    public List<String> getOptions() {
        return options;
    }

    public void setOptions(List<String> options) {
        this.options = options;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public boolean isDemonstrated() {
        return demonstrated;
    }

    public void setDemonstrated(boolean demonstrated) {
        this.demonstrated = demonstrated;
    }

    public boolean isTrigger() {
        return trigger;
    }

    public void setTrigger(boolean trigger) {
        this.trigger = trigger;
    }
}
