package com.sn.consumer.response.questionaire;



import com.sn.consumer.response.base.AbstractGenericResponse;

import java.util.List;

public class QuestionaireResponse extends AbstractGenericResponse {
    private List<QuestionResponse> questionResponseList;
    private String quesionaireDetail;
    private String quesionaireHeading;

    public String getQuesionaireHeading() {
        return quesionaireHeading;
    }

    public void setQuesionaireHeading(String quesionaireHeading) {
        this.quesionaireHeading = quesionaireHeading;
    }

    public String getQuesionaireDetail() {
        return quesionaireDetail;
    }

    public void setQuesionaireDetail(String quesionaireDetail) {
        this.quesionaireDetail = quesionaireDetail;
    }

    public List<QuestionResponse> getQuestionResponseList() {
        return questionResponseList;
    }

    public void setQuestionResponseList(List<QuestionResponse> questionResponseList) {
        this.questionResponseList = questionResponseList;
    }
}
