package com.sn.consumer.service;

import com.sn.consumer.response.advice.AdviceResponse;

public interface AdviceService {

    AdviceResponse getSystemAdvice(String questionnaireAnswerDocId, String skillAssessmentAnswerDocId);

}
