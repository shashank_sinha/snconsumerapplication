package com.sn.consumer.service;

import com.sn.consumer.entity.advice.AnalyticsPattern;
import com.sn.consumer.repository.AnalyticsPatternRepository;
import com.sn.consumer.request.advice.AnalyticsPatternRequest;
import com.sn.consumer.response.advice.AnalyticsPatternResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;


@Service
public class AnalyticsPatternServiceImpl implements AnalyticsPatternService {

    private final Logger log = LoggerFactory.getLogger(AnalyticsPatternServiceImpl.class);

    private final AnalyticsPatternRepository analyticsPatternRepository;

    public AnalyticsPatternServiceImpl(AnalyticsPatternRepository analyticsPatternRepository) {
        this.analyticsPatternRepository = analyticsPatternRepository;
    }

    @Override
    public AnalyticsPatternResponse save(AnalyticsPatternRequest analyticsPatternRequest) {
        log.debug("Request to save AnalyticsPattern : {}", analyticsPatternRequest);
        AnalyticsPattern analyticsPattern = new AnalyticsPattern(analyticsPatternRequest);
        analyticsPattern = analyticsPatternRepository.save(analyticsPattern);
        return new AnalyticsPatternResponse(analyticsPattern);
    }

    @Override
    public List<AnalyticsPatternResponse> findAll() {
        log.debug("Request to get all AnalyticsPatterns");
        List<AnalyticsPattern> analyticsPatternList = analyticsPatternRepository.findAll();
        List<AnalyticsPatternResponse> list = new ArrayList<>();
        for(AnalyticsPattern pattern : analyticsPatternList){
            list.add(new AnalyticsPatternResponse(pattern));
        }
        return list;
    }

    /**
     * Get one analyticsPattern by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    public AnalyticsPatternResponse findOne(String id) {
        log.debug("Request to get AnalyticsPattern : {}", id);
        return new AnalyticsPatternResponse(analyticsPatternRepository.findById(id).orElse(null));

    }

    /**
     * Delete the analyticsPattern by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(String id) {
        log.debug("Request to delete AnalyticsPattern : {}", id);
        analyticsPatternRepository.deleteById(id);
    }

    @Override
    public AnalyticsPatternResponse findByPatternId(String pattern) {
        log.debug("Request to delete AnalyticsPattern : {}", pattern);
        AnalyticsPattern analyticsPattern = analyticsPatternRepository.findByPattern(pattern);
        AnalyticsPatternResponse analyticsPatternResponse = new AnalyticsPatternResponse();
        analyticsPatternResponse.setId(analyticsPattern.getId());
        analyticsPatternResponse.setPattern(analyticsPattern.getPattern());
        return analyticsPatternResponse;
    }
}
