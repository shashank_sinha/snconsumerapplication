package com.sn.consumer.service;


import com.sn.consumer.request.advice.ScenarioAdviceRequest;
import com.sn.consumer.response.advice.ScenarioAdviceResponse;

import java.util.List;

/**
 * Service Interface for managing {@link com.herc.advices.domain.entity.ScenarioAdvice}.
 */
public interface ScenarioAdviceService {

    /**
     * Save a scenarioAdvice.
     *
     * @param scenarioAdviceDTO the entity to save.
     * @return the persisted entity.
     */
    ScenarioAdviceResponse save(ScenarioAdviceRequest scenarioAdviceDTO);

    /**
     * Get all the scenarioAdvices.
     *
     * @return the list of entities.
     */
    List<ScenarioAdviceResponse> findAll();


    /**
     * Get the "id" scenarioAdvice.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    ScenarioAdviceResponse findOne(String id);

    List<ScenarioAdviceResponse> findAllAdvicesByServiceId(String serviceId);

    /**
     * Delete the "id" scenarioAdvice.
     *
     * @param id the id of the entity.
     */
    void delete(String id);
}
