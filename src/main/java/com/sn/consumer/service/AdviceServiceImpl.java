package com.sn.consumer.service;

import com.sn.consumer.dto.AdviceDTO;
import com.sn.consumer.dto.AdviceDetail;
import com.sn.consumer.dto.AdviceLogDTO;
import com.sn.consumer.dto.ServicesDTO;
import com.sn.consumer.entity.Advice;
import com.sn.consumer.entity.UserAnswer;
import com.sn.consumer.response.advice.*;

import com.sn.consumer.response.questionaire.QuestionResponse;
import com.sn.consumer.response.questionaire.QuestionaireResponse;
import com.sn.consumer.utils.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;


import java.util.*;

import static java.time.Instant.EPOCH;

public class AdviceServiceImpl implements AdviceService {

    private final Logger log = LoggerFactory.getLogger(AdviceServiceImpl.class);

    @Autowired
    private ScenarioAdviceService scenarioAdviceService;

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private AnalyticsPatternService analyticsPatternService;

    @Value("${herc.app.base.url}")
    private String baseUrl;

    @Override
    public AdviceResponse getSystemAdvice(String questionnaireAnswerDocId, String skillAssessmentAnswerDocId) {
        populateScenarioAdvices(questionnaireAnswerDocId);
        populateArticleBankAdvices(questionnaireAnswerDocId);
        populateDataAnalyticsAdvices(skillAssessmentAnswerDocId);
        return null;
    }

    private void populateDataAnalyticsAdvices(String skillAssessmentAnswerDocId) {
        Map<String, Long> states = new HashMap<>();
        states.put("init", EPOCH.toEpochMilli());
        ResponseEntity<UserAnswer> skillAssessmentAnswers = restTemplate.getForEntity(baseUrl+"user/answer/"+skillAssessmentAnswerDocId, UserAnswer.class);
        String serviceId = skillAssessmentAnswers.getBody().getServiceId();
        String userId = skillAssessmentAnswers.getBody().getUserId();

        ResponseEntity<QuestionaireResponse> questionnaireListByServiceId = restTemplate.getForEntity(baseUrl+"questionaires/questions/"+skillAssessmentAnswers.getBody().getServiceId(), QuestionaireResponse.class);
        List<QuestionResponse> questionResponses = questionnaireListByServiceId.getBody().getQuestionResponseList();
        String questionId = null;
        // Create a map of MH, NTH, SME, CS in this order
        // Return first positive skill and its pattern
        // On the basis of
        String patternId = new String(); // TODO : To be fetched from skill matrix, hurdle is how to find what is MH, NTH, SME and CS
        AnalyticsPatternResponse analyticsPatternResponse = analyticsPatternService.findByPatternId(patternId);
        //analyticsPatternResponse
        List<UserAnswer.Answers> answers = skillAssessmentAnswers.getBody().getAnswers();
        String answerValue = null;
        for (UserAnswer.Answers answer : answers){
            if(answer.getQuestionId().equalsIgnoreCase(questionId)){
                answerValue = answer.getAnswer();
            }
        }
        List<AnalyticsAdviceResponse> analyticsAdvice = new ArrayList<>();

        ResponseEntity<AdviceLogDTO> userAdviceLog = restTemplate.getForEntity(baseUrl+"adviceLog/"+userId, AdviceLogDTO.class);
        states.put("processed", EPOCH.toEpochMilli());
        createAdviceLogForDataAnalyticsBank(analyticsAdvice, serviceId, userId, Optional.of(userAdviceLog.getBody()), states);
    }

    private void populateArticleBankAdvices(String questionnaireAnswerDocId) {
        Map<String, Long> states = new HashMap<>();
        states.put("init", EPOCH.toEpochMilli());
        ResponseEntity<UserAnswer> answerDocUsingAnswerDocId = restTemplate.getForEntity(baseUrl+"user/answer/"+questionnaireAnswerDocId, UserAnswer.class);
        String serviceId = answerDocUsingAnswerDocId.getBody().getServiceId();
        String userId = answerDocUsingAnswerDocId.getBody().getUserId();
        ResponseEntity<ServicesDTO> servicesDTO = restTemplate.getForEntity(baseUrl+"services/"+serviceId, ServicesDTO.class);
        List<AdviceDTO> advices = findAllArticleAdvices(serviceId);
        String mostNegativeSkill = new String(); // TODO : This needs to be populated from Skill matrix
        List<AdviceDTO> articleAdvices = new ArrayList<>();
        advices.forEach(adviceDTO -> {
            List<String> keywords = Arrays.asList(adviceDTO.getKeywords().split(","));
            Optional<String> negativeSkill = keywords.stream().filter(keyword -> keyword.equalsIgnoreCase(mostNegativeSkill)).findFirst();
            if(negativeSkill.isPresent()){
                articleAdvices.add(adviceDTO);
            }
        });
        ResponseEntity<AdviceLogDTO> userAdviceLog = restTemplate.getForEntity(baseUrl+"adviceLog/"+userId, AdviceLogDTO.class);
        states.put("processed", EPOCH.toEpochMilli());
        createAdviceLogForArticleBank(articleAdvices, serviceId, userId, Optional.of(userAdviceLog.getBody()), states);
        //return articleAdvices;
    }

    private void populateScenarioAdvices(String questionnaireAnswerDocId) {
        Map<String, Long> states = new HashMap<>();
        states.put("init", EPOCH.toEpochMilli());
        ResponseEntity<UserAnswer> answerDocUsingAnswerDocId = restTemplate.getForEntity(baseUrl+"user/answer/"+questionnaireAnswerDocId, UserAnswer.class);
        String serviceId = answerDocUsingAnswerDocId.getBody().getServiceId();
        String userId = answerDocUsingAnswerDocId.getBody().getUserId();

        ResponseEntity<QuestionaireResponse> questionnaireListByServiceId = restTemplate.getForEntity(baseUrl+"questionaires/questions/"+answerDocUsingAnswerDocId.getBody().getServiceId(), QuestionaireResponse.class);
        List<QuestionResponse> questionResponses = questionnaireListByServiceId.getBody().getQuestionResponseList();
        String questionId = null;
        for (QuestionResponse questionResponse : questionResponses){
            if(questionResponse.isTrigger()){
                questionId = questionResponse.getQuestionId();
                break;
            }
        }

        List<UserAnswer.Answers> answers = answerDocUsingAnswerDocId.getBody().getAnswers();
        String answerValue = null;
        for (UserAnswer.Answers answer : answers){
            if(answer.getQuestionId().equalsIgnoreCase(questionId)){
                answerValue = answer.getAnswer();
            }
        }

        ScenarioAdviceResponse scenarioAdviceResponse = null;
        List<ScenarioAdviceResponse> allAdvicesForService = findAllScenarioAdvices(serviceId);
        for (ScenarioAdviceResponse scenarioAdvice : allAdvicesForService){
            if(scenarioAdvice.getTriggersValue() != null && scenarioAdvice.getTriggersValue().equalsIgnoreCase(answerValue)){
                scenarioAdviceResponse = scenarioAdvice;
            }
        }

        ResponseEntity<AdviceLogDTO> userAdviceLog = restTemplate.getForEntity(baseUrl+"adviceLog/"+userId, AdviceLogDTO.class);
        states.put("processed", EPOCH.toEpochMilli());
        createScenarioAdviceLog(scenarioAdviceResponse, serviceId, userId, Optional.ofNullable(userAdviceLog.getBody()), states);

        // return adviceLogDTO;

    }

    private void createScenarioAdviceLog(ScenarioAdviceResponse scenarioAdviceResponse, String serviceId, String userId, Optional<AdviceLogDTO> userAdviceLog, Map<String, Long> states){
        AdviceLogDTO adviceLogDTO = null;
        if(userAdviceLog.isPresent()){
            adviceLogDTO = userAdviceLog.get(); // Updating existing advice log
        }else{
            adviceLogDTO = new AdviceLogDTO(); // Creating new advice log
        }
        adviceLogDTO.setUserId(userId);
        adviceLogDTO.setServiceId(serviceId);
        adviceLogDTO.setCreatedDate(new Date());
        AdviceDetail adviceDetail = new AdviceDetail();
        adviceDetail.setName(scenarioAdviceResponse.getName());
        adviceDetail.setDescription(scenarioAdviceResponse.getDescription());
        adviceDetail.setThumbnail(scenarioAdviceResponse.getThumbnailUrl());
        adviceLogDTO.setAdvices(Arrays.asList(adviceDetail));
        AdviceBank adviceBank = new AdviceBank();
        adviceBank.setPriority("1");
        adviceBank.setSourceId("Scenario Bank");
        adviceBank.setAdviceId(scenarioAdviceResponse.getId());
        adviceLogDTO.setAdviceBanks(Arrays.asList(adviceBank));
        adviceLogDTO.setStates(states);
        ResponseEntity<AdviceLogDTO> adviceLogDTOResponseEntity = restTemplate.postForEntity(baseUrl+"adviceLog/", adviceLogDTO, AdviceLogDTO.class);
    }

    private void createAdviceLogForDataAnalyticsBank(List<AnalyticsAdviceResponse> analyticsAdvice, String serviceId, String userId, Optional<AdviceLogDTO> userAdviceLog, Map<String, Long> states) {
        AdviceLogDTO adviceLogDTO = null;
        List<AdviceBank> adviceBanks = new ArrayList<>();
        if(userAdviceLog.isPresent()){
            adviceLogDTO = userAdviceLog.get(); // Updating existing advice log
        }else{
            adviceLogDTO = new AdviceLogDTO(); // Creating new advice log
        }
        List<AdviceDetail> adviceDetails = new ArrayList<>();
        analyticsAdvice.forEach(adviceDTO -> {
            AdviceDetail adviceDetail = new AdviceDetail();
            adviceDetail.setName(adviceDTO.getName());
            adviceDetail.setDescription(adviceDTO.getDescription());
            adviceDetail.setThumbnail(adviceDTO.getThumbnailUrl());
            adviceDetails.add(adviceDetail);
            AdviceBank adviceBank = new AdviceBank();
            adviceBank.setAdviceId(adviceDTO.getId());
            adviceBank.setSourceId("Article Bank");
            adviceBank.setPriority("1");
            adviceBanks.add(adviceBank);
        });
        adviceLogDTO.setAdvices(adviceDetails);
        adviceLogDTO.setServiceId(serviceId);
        adviceLogDTO.setUserId(userId);
        adviceLogDTO.setCreatedDate(new Date());
        adviceLogDTO.setStates(states);
        adviceLogDTO.setAdviceBanks(adviceBanks);
        ResponseEntity<AdviceLogDTO> adviceLogDTOResponseEntity = restTemplate.postForEntity(baseUrl+"adviceLog/", adviceLogDTO, AdviceLogDTO.class);

    }

    private void createAdviceLogForArticleBank(List<AdviceDTO> advices, String serviceId, String userId, Optional<AdviceLogDTO> userAdviceLog, Map<String, Long> states){
        AdviceLogDTO adviceLogDTO = null;
        List<AdviceBank> adviceBanks = new ArrayList<>();
        if(userAdviceLog.isPresent()){
            adviceLogDTO = userAdviceLog.get(); // Updating existing advice log
        }else{
            adviceLogDTO = new AdviceLogDTO(); // Creating new advice log
        }
        List<AdviceDetail> adviceDetails = new ArrayList<>();
        advices.forEach(adviceDTO -> {
            AdviceDetail adviceDetail = new AdviceDetail();
            adviceDetail.setName(adviceDTO.getName());
            adviceDetail.setDescription(adviceDTO.getDescription());
            adviceDetail.setThumbnail(adviceDTO.getThumbnail());
            adviceDetail.setType(adviceDTO.getType());
            adviceDetail.setUrl(adviceDTO.getUrl());
            adviceDetails.add(adviceDetail);
            AdviceBank adviceBank = new AdviceBank();
            adviceBank.setAdviceId(adviceDTO.getId());
            adviceBank.setSourceId("Article Bank");
            adviceBank.setPriority("1");
            adviceBanks.add(adviceBank);
        });
        adviceLogDTO.setAdvices(adviceDetails);
        adviceLogDTO.setServiceId(serviceId);
        adviceLogDTO.setUserId(userId);
        adviceLogDTO.setCreatedDate(new Date());
        adviceLogDTO.setStates(states);
        adviceLogDTO.setAdviceBanks(adviceBanks);
        ResponseEntity<AdviceLogDTO> adviceLogDTOResponseEntity = restTemplate.postForEntity(baseUrl+"adviceLog/", adviceLogDTO, AdviceLogDTO.class);
        if(adviceLogDTOResponseEntity == null){
            log.error("Error creating Advice Log for user {} having serviceId {} ", userId, serviceId);
        }else{
            log.info("Advice Log created successfully for user {} having serviceId {} ", userId, serviceId);
        }
    }

    public List<AdviceDTO> findAllArticleAdvices(String serviceId){
        ResponseEntity<List<AdviceDTO>> adviceList = restTemplate.exchange(baseUrl+"advices/"+serviceId,
                HttpMethod.GET,null, new ParameterizedTypeReference<List<AdviceDTO>>(){});
        return adviceList.getBody();
    }

    public List<ScenarioAdviceResponse> findAllScenarioAdvices(String serviceId){
        List<ScenarioAdviceResponse> scenarioAdvicesByServiceId = scenarioAdviceService.findAllAdvicesByServiceId(serviceId);
        return scenarioAdvicesByServiceId;
    }

    private boolean checkIfLeader(String answerDocId, ResponseEntity<UserAnswer> answerDocUsingAnswerDocId) {

        // GET All Questions & Answers
        List<UserAnswer.Answers> answers = answerDocUsingAnswerDocId.getBody().getAnswers();

        // Prepare Map of questionId ans Answer
        Map<String, UserAnswer.Answers> questionIdAnswerMap =
                Utils.toMapBy(answers, UserAnswer.Answers::getQuestionId);

        // GET All questionOfService
        ResponseEntity<QuestionaireResponse> questionnaireListByServiceId = restTemplate.getForEntity(baseUrl+"questionaires/questions/"+answerDocUsingAnswerDocId.getBody().getServiceId(), QuestionaireResponse.class);
        return checkIfQuestionsContainsLeaderQuestionAndMarkedAsLeader(
                questionnaireListByServiceId.getBody().getQuestionResponseList(), questionIdAnswerMap);
    }

    boolean checkIfQuestionsContainsLeaderQuestionAndMarkedAsLeader(List<QuestionResponse> questionResponseList,
                                                                    Map<String, UserAnswer.Answers> questionIdAnswerMap) {
        for (QuestionResponse perQuestion : questionResponseList) {
            if (perQuestion.isLeaderQuestion()) {
                if (questionIdAnswerMap.containsKey(perQuestion.getQuestionId())) {
                    UserAnswer.Answers answers = questionIdAnswerMap.get(perQuestion.getQuestionId());
                    if (answers.getAnswer().equalsIgnoreCase("true")) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    private AdviceDTO getDefaultAdvice(){
        AdviceDTO adviceDTO = new AdviceDTO();
        adviceDTO.setName("Default Advice");
        adviceDTO.setDescription("No advice available for this servie at the moment. Your request has been logged. We will build personalised advice for you and will notify you once is done!");
        return adviceDTO;
    }
}
