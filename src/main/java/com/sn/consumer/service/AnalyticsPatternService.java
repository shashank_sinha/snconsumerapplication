package com.sn.consumer.service;

import com.sn.consumer.request.advice.AnalyticsPatternRequest;
import com.sn.consumer.response.advice.AnalyticsPatternResponse;

import java.util.List;

/**
 * Service Interface for managing {@link com.herc.advices.domain.entity.AnalyticsPattern}.
 */
public interface AnalyticsPatternService {

    /**
     * Save a analyticsPattern.
     *
     * @param analyticsPatternDTO the entity to save.
     * @return the persisted entity.
     */
    AnalyticsPatternResponse save(AnalyticsPatternRequest analyticsPatternDTO);

    List<AnalyticsPatternResponse> findAll();

    /**
     * Get the "id" analyticsPattern.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    AnalyticsPatternResponse findOne(String id);

    /**
     * Delete the "id" analyticsPattern.
     *
     * @param id the id of the entity.
     */
    void delete(String id);

    AnalyticsPatternResponse findByPatternId(String patternId);
}
