package com.sn.consumer.service;

import com.sn.consumer.entity.Services;
import com.sn.consumer.entity.advice.ScenarioAdvice;
import com.sn.consumer.repository.ScenarioAdviceRepository;
import com.sn.consumer.repository.TriggerRepository;
import com.sn.consumer.request.advice.ScenarioAdviceRequest;
import com.sn.consumer.response.advice.ScenarioAdviceResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

@Service
public class ScenarioAdviceServiceImpl implements ScenarioAdviceService {

    private final Logger log = LoggerFactory.getLogger(ScenarioAdviceServiceImpl.class);

    private final ScenarioAdviceRepository scenarioAdviceRepository;

    @Autowired
    private TriggerRepository triggerRepository;

    @Value("${herc.app.base.url}")
    private String baseUrl;

    @Autowired
    private RestTemplate restTemplate;

    public ScenarioAdviceServiceImpl(ScenarioAdviceRepository scenarioAdviceRepository) {
        this.scenarioAdviceRepository = scenarioAdviceRepository;
    }

    /**
     * Save a scenarioAdvice.
     *y
     * @param scenarioAdviceRequest the entity to save.
     * @return the persisted entity.
     */
    @Override
    public ScenarioAdviceResponse save(ScenarioAdviceRequest scenarioAdviceRequest) {
        log.debug("Request to save ScenarioAdvice : {}", scenarioAdviceRequest);
        ScenarioAdvice scenarioAdvice = new ScenarioAdvice(scenarioAdviceRequest);
        scenarioAdvice = scenarioAdviceRepository.save(scenarioAdvice);
        scenarioAdvice.setServices(restTemplate.getForEntity(baseUrl+"services/"+scenarioAdviceRequest.getServicesId(), Services.class).getBody());
        scenarioAdvice.setTriggers(triggerRepository.findById(scenarioAdvice.getTriggersId()).orElse(null));
        return new ScenarioAdviceResponse(scenarioAdvice);
    }

    /**
     * Get all the scenarioAdvices.
     *
     * @return the list of entities.
     */
    @Override
    public List<ScenarioAdviceResponse> findAll() {
        log.debug("Request to get all ScenarioAdvices");
        List<ScenarioAdvice> adviceList = scenarioAdviceRepository.findAll();
        List<ScenarioAdviceResponse> list = new ArrayList<>();
        for(ScenarioAdvice advice : adviceList){
            advice.setServices(restTemplate.getForEntity(baseUrl+"services/"+advice.getServicesId(), Services.class).getBody());
            list.add(new ScenarioAdviceResponse(advice));
        }
        return list;
    }

    /**
     * Get one scenarioAdvice by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    public ScenarioAdviceResponse findOne(String id) {
        log.debug("Request to get ScenarioAdvice : {}", id);
        ScenarioAdvice scenarioAdvice = scenarioAdviceRepository.findById(id).orElse(null);
        scenarioAdvice.setServices(restTemplate.getForEntity(baseUrl+"services/"+scenarioAdvice.getServicesId(), Services.class).getBody());
        return new ScenarioAdviceResponse(scenarioAdvice);
    }

    @Override
    public List<ScenarioAdviceResponse> findAllAdvicesByServiceId(String serviceId) {
        List<ScenarioAdvice> adviceList = scenarioAdviceRepository.findByServicesId(serviceId);
        List<ScenarioAdviceResponse> scenarioAdviceResponses = new ArrayList<>();
        adviceList.forEach(scenarioAdvice -> {
            ScenarioAdviceResponse scenarioAdviceResponse = new ScenarioAdviceResponse();
            scenarioAdviceResponse.setName(scenarioAdvice.getName());
            scenarioAdviceResponse.setDescription(scenarioAdvice.getDescription());
            scenarioAdviceResponse.setThumbnailUrl(scenarioAdvice.getThumbnailUrl());
            scenarioAdviceResponse.setId(scenarioAdvice.getId());
            scenarioAdviceResponse.setTriggersId(scenarioAdvice.getTriggersId());
            scenarioAdviceResponse.setTriggersValue(scenarioAdvice.getTriggers().getValue());
            scenarioAdviceResponses.add(scenarioAdviceResponse);
        });
        return scenarioAdviceResponses;
    }

    /**
     * Delete the scenarioAdvice by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(String id) {
        log.debug("Request to delete ScenarioAdvice : {}", id);
        scenarioAdviceRepository.deleteById(id);
    }
}
