package com.sn.consumer.dto;


import com.sn.consumer.entity.Services;

import java.io.Serializable;
import java.util.List;

public class ServicesDTO implements Serializable {

    private String id;

    //@NotNull
    private String name;

    //@NotNull
    private Integer level;

    private Integer priority;

    private String description;

    private String adviceDetail;

    //@NotNull
    private Boolean active;

    //@NotNull
    private Integer referenceId;

    private String language;

    private String parentServiceId;

    private String parentServiceName;

    private List<String> questionsList;
    private List<String> skillAssessmentQuestionsList;

    private String logoURL;

    private String questionnaireDescription;
    private String questionnaireHeading;

    public ServicesDTO() {

    }

    public ServicesDTO(Services services) {
        this.id = (services.getId());
        this.name = (services.getName());
        this.active = (services.isActive());
        this.level = services.getLevel();
        this.referenceId = services.getReferenceId();
        this.language = services.getLanguage();
        this.logoURL = services.getLogoURL();
        this.description = services.getDescription();
        this.questionnaireDescription = services.getQuestionnaireDescription();
        this.questionnaireHeading = services.getQuestionnaireHeading();
        this.adviceDetail = services.getAdviceDetail();
        if(services.getParentService() != null) {
            this.parentServiceId = services.getParentService().getId();
            this.parentServiceName = services.getParentService().getName();
        }
        this.priority = services.getPriority();
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public String getQuestionnaireHeading() {
        return questionnaireHeading;
    }

    public void setQuestionnaireHeading(String questionnaireHeading) {
        this.questionnaireHeading = questionnaireHeading;
    }

    public List<String> getSkillAssessmentQuestionsList() {
        return skillAssessmentQuestionsList;
    }

    public void setSkillAssessmentQuestionsList(List<String> skillAssessmentQuestionsList) {
        this.skillAssessmentQuestionsList = skillAssessmentQuestionsList;
    }

    public List<String> getQuestionsList() {
        return questionsList;
    }

    public void setQuestionsList(List<String> questionsList) {
        this.questionsList = questionsList;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean isActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public String getParentServiceId() {
        return parentServiceId;
    }

    public void setParentServiceId(String servicesId) {
        this.parentServiceId = servicesId;
    }

    public String getParentServiceName() {
        return parentServiceName;
    }

    public void setParentServiceName(String servicesName) {
        this.parentServiceName = servicesName;
    }

    public String getLogoURL() {
        return logoURL;
    }

    public void setLogoURL(String logoURL) {
        this.logoURL = logoURL;
    }

    public Boolean getActive() {
        return active;
    }

    public String getQuestionnaireDescription() {
        return questionnaireDescription;
    }

    public void setQuestionnaireDescription(String questionnaireDescription) {
        this.questionnaireDescription = questionnaireDescription;
    }

    public Integer getReferenceId() {
        return referenceId;
    }

    public void setReferenceId(Integer referenceId) {
        this.referenceId = referenceId;
    }


    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getAdviceDetail() {
        return adviceDetail;
    }

    public void setAdviceDetail(String adviceDetail) {
        this.adviceDetail = adviceDetail;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ServicesDTO)) {
            return false;
        }

        return id != null && id.equals(((ServicesDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ServicesDTO{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", level=" + getLevel() +
            ", description='" + getDescription() + "'" +
            ", adviceDetail='" + getAdviceDetail() + "'" +
            ", active='" + isActive() + "'" +
            ", parentServiceId='" + getParentServiceId() + "'" +
            ", parentServiceName='" + getParentServiceName() + "'" +
            "}";
    }
}
