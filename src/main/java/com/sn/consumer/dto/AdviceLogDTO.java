package com.sn.consumer.dto;

import com.sn.consumer.response.advice.AdviceBank;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class AdviceLogDTO implements Serializable {

    private String id;

    private String userId;

    private String questionnaireAnswerId;

    private String skillAssessmentAnswerId;

    private List<AdviceDetail> advices;

    private List<AdviceBank> adviceBanks;

    private String serviceId;

    private Date createdDate;

    private Map states;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public List<AdviceDetail> getAdvices() {
        return advices;
    }

    public void setAdvices(List<AdviceDetail> advices) {
        this.advices = advices;
    }

    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getQuestionnaireAnswerId() {
        return questionnaireAnswerId;
    }

    public void setQuestionnaireAnswerId(String questionnaireAnswerId) {
        this.questionnaireAnswerId = questionnaireAnswerId;
    }

    public String getSkillAssessmentAnswerId() {
        return skillAssessmentAnswerId;
    }

    public void setSkillAssessmentAnswerId(String skillAssessmentAnswerId) {
        this.skillAssessmentAnswerId = skillAssessmentAnswerId;
    }

    public Map getStates() {
        return states;
    }

    public void setStates(Map states) {
        this.states = states;
    }

    public List<AdviceBank> getAdviceBanks() {
        return adviceBanks;
    }

    public void setAdviceBanks(List<AdviceBank> adviceBanks) {
        this.adviceBanks = adviceBanks;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        AdviceLogDTO that = (AdviceLogDTO) o;

        return new EqualsBuilder()
            .append(id, that.id)
            .append(userId, that.userId)
            .append(advices, that.advices)
            .append(questionnaireAnswerId, that.questionnaireAnswerId)
            .append(skillAssessmentAnswerId, that.skillAssessmentAnswerId)
            .append(adviceBanks, that.adviceBanks)
            .append(serviceId, that.serviceId)
            .append(createdDate, that.createdDate)
            .append(states, that.states)
            .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
            .append(id)
            .append(userId)
            .append(advices)
            .append(questionnaireAnswerId)
            .append(skillAssessmentAnswerId)
            .append(adviceBanks)
            .append(serviceId)
            .append(createdDate)
            .append(states)
            .toHashCode();
    }

    @Override
    public String toString() {
        return "AdviceLogDTO{" +
            "id='" + id + '\'' +
            ", userId='" + userId + '\'' +
            ", advices='" + advices + '\'' +
            ", questionnaireAnswerId='" + questionnaireAnswerId + '\'' +
            ", skillAssessmentAnswerId='" + skillAssessmentAnswerId + '\'' +
            ", adviceBanks=" + adviceBanks +
            ", serviceId='" + serviceId + '\'' +
            ", createdDate=" + createdDate +
            ", states=" + states +
            '}';
    }
}
