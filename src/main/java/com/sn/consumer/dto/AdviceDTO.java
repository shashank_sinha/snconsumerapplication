package com.sn.consumer.dto;


import com.sn.consumer.entity.Advice;

import java.io.Serializable;

public class AdviceDTO implements Serializable {

    private String id;

    private String name;

    private String description;

    private String type;

    private String url;

    private String thumbnail;

    private String keywords;

    private String language;

    private Integer reference_id;

    private String created_by;

    private String priority;

    private String job_role_category;

    private Boolean active;


    private String servicesId;

    private String servicesName;

    public AdviceDTO(Advice advice) {
        this.id = advice.getId();
        this.name = advice.getName();
        this.description = advice.getDescription();
        this.type = advice.getType();
        this.url = advice.getUrl();
        this.thumbnail = advice.getThumbnail();
        this.keywords = advice.getKeywords();
        this.language = advice.getLanguage();
        this.reference_id = advice.getReference_id();
        this.created_by = advice.getCreated_by();
        this.priority = advice.getPriority();
        this.job_role_category = advice.getJob_role_category();
        this.active = advice.getActive();
        this.servicesId = advice.getServicesId();
    }

    public AdviceDTO() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getKeywords() {
        return keywords;
    }

    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public Integer getReference_id() {
        return reference_id;
    }

    public void setReference_id(Integer reference_id) {
        this.reference_id = reference_id;
    }

    public String getCreated_by() {
        return created_by;
    }

    public void setCreated_by(String created_by) {
        this.created_by = created_by;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public String getJob_role_category() {
        return job_role_category;
    }

    public void setJob_role_category(String job_role_category) {
        this.job_role_category = job_role_category;
    }

    public Boolean isActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public String getServicesId() {
        return servicesId;
    }

    public void setServicesId(String servicesId) {
        this.servicesId = servicesId;
    }

    public String getServicesName() {
        return servicesName;
    }

    public void setServicesName(String servicesName) {
        this.servicesName = servicesName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof AdviceDTO)) {
            return false;
        }

        return id != null && id.equals(((AdviceDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "AdviceDTO{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", description='" + getDescription() + "'" +
            ", type='" + getType() + "'" +
            ", url='" + getUrl() + "'" +
            ", thumbnail='" + getThumbnail() + "'" +
            ", keywords='" + getKeywords() + "'" +
            ", language='" + getLanguage() + "'" +
            ", reference_id=" + getReference_id() +
            ", created_by='" + getCreated_by() + "'" +
            ", priority='" + getPriority() + "'" +
            ", job_role_category='" + getJob_role_category() + "'" +
            ", active='" + isActive() + "'" +
            ", servicesId='" + getServicesId() + "'" +
            ", servicesName='" + getServicesName() + "'" +
            "}";
    }
}
