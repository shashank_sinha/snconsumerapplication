package com.sn.consumer.dto;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

public class AdviceDetail {

    private String name;

    private String description;

    private String thumbnail;

    private String type;

    private String url;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        AdviceDetail that = (AdviceDetail) o;

        return new EqualsBuilder()
            .append(name, that.name)
            .append(description, that.description)
            .append(thumbnail, that.thumbnail)
            .append(type, that.type)
            .append(url, that.url)
            .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
            .append(name)
            .append(description)
            .append(thumbnail)
            .append(type)
            .append(url)
            .toHashCode();
    }

    @Override
    public String toString() {
        return "AdviceDetail{" +
            "name='" + name + '\'' +
            ", description='" + description + '\'' +
            ", thumbnail='" + thumbnail + '\'' +
            ", type='" + type + '\'' +
            ", url='" + url + '\'' +
            '}';
    }
}
