package com.sn.consumer.controller.base;

import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public interface IController {
    void doProcessing(String msg) throws IOException;
}
