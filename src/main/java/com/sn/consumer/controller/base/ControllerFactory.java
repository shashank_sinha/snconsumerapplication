package com.sn.consumer.controller.base;

import com.sn.consumer.constants.Constants;
import com.sn.consumer.controller.AdviceProcessingController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Service;

@Service
@ComponentScan(basePackages = "com.sn.consumer.*")
public class ControllerFactory {

    @Autowired
    private AdviceProcessingController adviceProcessingController;

    public IController getController(String control) {
        IController controller = null;

        switch (control) {
            case Constants.ADVICE_PROCESSING:
                controller = adviceProcessingController;
                break;
            default:
                controller=null;
        }

        return controller;
    }

}
