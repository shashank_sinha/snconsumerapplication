package com.sn.consumer.controller;


import com.sn.consumer.controller.base.IController;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
@Slf4j
public class AdviceProcessingController implements IController {
    @Override
    public void doProcessing(final String msg) throws IOException {
        log.info("Message being processed :: "+msg);
    }

}
