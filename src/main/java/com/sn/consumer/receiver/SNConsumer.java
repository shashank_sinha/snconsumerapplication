package com.sn.consumer.receiver;

import com.sn.consumer.controller.base.ControllerFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
@Slf4j
public class SNConsumer {

    @Autowired
    ControllerFactory controllerFactory;

    @Value("${application.consumer.control.advice}")
    private String adviceControl;

    @KafkaListener(topics = "${spring.kafka.advice.topic}", groupId = "group_id")
    public void doAdviceProcessing(String message) {
        log.info(String.format("#### -> Consumed message -> %s", message));
        try {
            controllerFactory.getController(adviceControl).doProcessing(message);
        } catch (Exception e) {
            log.error("Exception encountered while processing the message :: " + message,e);
        }
    }

}
