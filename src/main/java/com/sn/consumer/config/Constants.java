package com.sn.consumer.config;

/**
 * Application constants.
 */
public final class Constants {

    public static final String SYSTEM_ACCOUNT = "system";
    public static final String ID_DELIMITER = "__";
    public static final int MAX_ADVICES = 3;

    private Constants() {
    }
}
