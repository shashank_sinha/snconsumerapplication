package com.sn.consumer.repository;

import com.sn.consumer.entity.advice.Trigger;
import org.springframework.stereotype.Repository;

/**
 * Spring Data Couchbase repository for the Trigger entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TriggerRepository extends N1qlCouchbaseRepository<Trigger, String> {
    Trigger findOneById(String id);
}
