package com.sn.consumer.repository;

import com.sn.consumer.entity.advice.AnalyticsPattern;
import org.springframework.stereotype.Repository;


@SuppressWarnings("unused")
@Repository
public interface AnalyticsPatternRepository extends N1qlCouchbaseRepository<AnalyticsPattern, String> {

    AnalyticsPattern findByPattern(String pattern);

}
