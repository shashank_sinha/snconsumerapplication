package com.sn.consumer.repository;

import com.sn.consumer.entity.advice.ScenarioAdvice;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data Couchbase repository for the ScenarioAdvice entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ScenarioAdviceRepository extends N1qlCouchbaseRepository<ScenarioAdvice, String> {
    List<ScenarioAdvice> findByServicesId(String serviceId);
}
