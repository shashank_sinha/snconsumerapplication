package com.sn.consumer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SNConsumerApplicationStarter {

    public static void main(String[] args) {
        SpringApplication.run(SNConsumerApplicationStarter.class, args);
    }
}
