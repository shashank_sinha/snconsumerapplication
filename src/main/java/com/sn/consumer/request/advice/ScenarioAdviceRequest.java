package com.sn.consumer.request.advice;


import com.sn.consumer.response.base.AbstractGenericResponse;

import javax.validation.constraints.NotNull;

/**
 * A DTO for the {@link com.herc.advices.domain.entity.ScenarioAdvice} entity.
 */
public class ScenarioAdviceRequest extends AbstractGenericResponse {

    private String id;

    @NotNull
    private String name;

    @NotNull
    private String description;

    @NotNull
    private String advice;

    @NotNull
    private String variable;

    @NotNull
    private String thumbnailUrl;

    @NotNull
    private String keywords;

    private String language;

    private Integer priority;

    @NotNull
    private Boolean active;

    @NotNull
    private String createdBy;

    @NotNull
    private Integer referenceId;


    private String servicesId;

    private String servicesName;

    private String triggersId;

    private String triggersValue;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getVariable() {
        return variable;
    }

    public void setVariable(String variable) {
        this.variable = variable;
    }

    public String getThumbnailUrl() {
        return thumbnailUrl;
    }

    public void setThumbnailUrl(String thumbnailUrl) {
        this.thumbnailUrl = thumbnailUrl;
    }

    public String getKeywords() {
        return keywords;
    }

    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public Boolean isActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Integer getReferenceId() {
        return referenceId;
    }

    public void setReferenceId(Integer referenceId) {
        this.referenceId = referenceId;
    }

    public String getServicesId() {
        return servicesId;
    }

    public void setServicesId(String servicesId) {
        this.servicesId = servicesId;
    }

    public String getServicesName() {
        return servicesName;
    }

    public void setServicesName(String servicesName) {
        this.servicesName = servicesName;
    }

    public String getTriggersId() {
        return triggersId;
    }

    public void setTriggersId(String triggerId) {
        this.triggersId = triggerId;
    }

    public String getTriggersValue() {
        return triggersValue;
    }

    public void setTriggersValue(String triggerValue) {
        this.triggersValue = triggerValue;
    }

    public String getAdvice() {
        return advice;
    }

    public void setAdvice(String advice) {
        this.advice = advice;
    }

    public Boolean getActive() {
        return active;
    }
}
