package com.sn.consumer.request.advice;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

public class TriggerRequest implements Serializable {

    private String id;

    @NotNull
    private String value;

    @NotNull
    private Integer referenceId;

    @NotNull
    private String createdBy;

    @NotNull
    private String language;

    @NotNull
    private Boolean active;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Integer getReferenceId() {
        return referenceId;
    }

    public void setReferenceId(Integer referenceId) {
        this.referenceId = referenceId;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public Boolean isActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

}
