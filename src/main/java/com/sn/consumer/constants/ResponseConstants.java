package com.sn.consumer.constants;

public class ResponseConstants {
    public static final String EXCEPTION = "exception";
    public static final String MESSAGES = "message";

    public static final String SERVICES_LIST = "serviceList";

}
