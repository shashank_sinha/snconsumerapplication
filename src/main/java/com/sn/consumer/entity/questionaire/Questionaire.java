package com.sn.consumer.entity.questionaire;

import com.couchbase.client.java.repository.annotation.Field;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.sn.consumer.entity.Services;
import org.springframework.data.annotation.Id;
import org.springframework.data.couchbase.core.mapping.Document;
import org.springframework.data.couchbase.core.mapping.id.GeneratedValue;
import org.springframework.data.couchbase.core.mapping.id.IdPrefix;
import org.springframework.data.couchbase.core.query.FetchType;
import org.springframework.data.couchbase.core.query.N1qlJoin;

import java.io.Serializable;

import static com.sn.consumer.config.Constants.ID_DELIMITER;
import static org.springframework.data.couchbase.core.mapping.id.GenerationStrategy.UNIQUE;

/**
 * A Questionaire.
 */
@Document
public class Questionaire implements Serializable {

    private static final long serialVersionUID = 1L;
    public static final String PREFIX = "questionaire";

    @SuppressWarnings("unused")
    @IdPrefix
    private String prefix = PREFIX;

    @Id
    @GeneratedValue(strategy = UNIQUE, delimiter = ID_DELIMITER)
    private String id;

    //@NotNull
    @Field("text")
    private String text;

    //@NotNull
    @Field("active")
    private Boolean active;

    @Field("reference_id")
    private Integer referenceId;

    @Field("language")
    private String language;

    //@NotNull
    @Field("skillAssessment")
    private Boolean skillAssessment;

    //@NotNull
    @Field("priority")
    private Integer priority;

    @Field("numberOfTextFields")
    private Integer numberOfTextFields;
    @Field("numberOfStaticFields")
    private Integer numberOfStaticFields;
    @Field("numberOfLookupFields")
    private Integer numberOfLookupFields;

    @Field("category")
    private String category;

    @Field("rating")
    private String rating;

    @Field("demonstrated")
    private Boolean demonstrated;

    @Field("leaderQuestion")
    private Boolean leaderQuestion;

    @Field("organisationQuestion")
    private Boolean organisationQuestion;

    @Field("jobRoleQuestion")
    private Boolean jobRoleQuestion;

    @Field("sub_category")
    private String subCategory;

    @Field("questiontype")
    private String questiontypeId;

    @N1qlJoin(on = "lks.questiontype=meta(rks).id", fetchType = FetchType.IMMEDIATE)
    @JsonIgnoreProperties(value = "questionaires", allowSetters = true)
    private Questiontype questiontype;

    @Field("service")
    private String serviceId;

    @N1qlJoin(on = "lks.service=meta(rks).id", fetchType = FetchType.IMMEDIATE)
    @JsonIgnoreProperties(value = "questionaires", allowSetters = true)
    private Services service;

    @Field("parentQuestion")
    private String parentQuestionId;
    @Field("childQuestion")
    private String childQuestionId;

    @N1qlJoin(on = "lks.parentQuestion=meta(rks).id", fetchType = FetchType.IMMEDIATE)
    @JsonIgnoreProperties(value = "questionaires", allowSetters = true)
    private Questionaire parentQuestion;

    @N1qlJoin(on = "lks.parentQuestion=meta(rks).id", fetchType = FetchType.IMMEDIATE)
    @JsonIgnoreProperties(value = "questionaires", allowSetters = true)
    private Questionaire childQuestion;

    @Field("mdCustomLookupId")
    private String mdCustomLookupId;
    private String mdCustomStaticOptionsId;
    private String mdMasterStaticOptionsId;

    @Field("mdMasterLookupId")
    private String mdMasterLookupId;

    @N1qlJoin(on = "lks.mdQuestionOptions=meta(rks).id", fetchType = FetchType.IMMEDIATE)
    @JsonIgnoreProperties(value = "questionaires", allowSetters = true)
    private MdQuestionOptions mdQuestionOptions;

    // jhipster-needle-entity-add-field - JHipster will add fields here


    public String getMdCustomStaticOptionsId() {
        return mdCustomStaticOptionsId;
    }

    public void setMdCustomStaticOptionsId(String mdCustomStaticOptionsId) {
        this.mdCustomStaticOptionsId = mdCustomStaticOptionsId;
    }

    public String getMdMasterStaticOptionsId() {
        return mdMasterStaticOptionsId;
    }

    public void setMdMasterStaticOptionsId(String mdMasterStaticOptionsId) {
        this.mdMasterStaticOptionsId = mdMasterStaticOptionsId;
    }

    public Boolean getLeaderQuestion() {
        return leaderQuestion;
    }

    public void setLeaderQuestion(Boolean leaderQuestion) {
        this.leaderQuestion = leaderQuestion;
    }

    public Boolean getOrganisationQuestion() {
        return organisationQuestion;
    }

    public void setOrganisationQuestion(Boolean organisationQuestion) {
        this.organisationQuestion = organisationQuestion;
    }

    public Boolean getJobRoleQuestion() {
        return jobRoleQuestion;
    }

    public void setJobRoleQuestion(Boolean jobRoleQuestion) {
        this.jobRoleQuestion = jobRoleQuestion;
    }

    public Boolean getActive() {
        return active;
    }

    public Boolean getSkillAssessment() {
        return skillAssessment;
    }

    public void setSkillAssessment(Boolean skillAssessment) {
        this.skillAssessment = skillAssessment;
    }

    public Integer getNumberOfTextFields() {
        return numberOfTextFields;
    }

    public void setNumberOfTextFields(Integer numberOfTextFields) {
        this.numberOfTextFields = numberOfTextFields;
    }

    public Integer getNumberOfStaticFields() {
        return numberOfStaticFields;
    }

    public void setNumberOfStaticFields(Integer numberOfStaticFields) {
        this.numberOfStaticFields = numberOfStaticFields;
    }

    public Integer getNumberOfLookupFields() {
        return numberOfLookupFields;
    }

    public void setNumberOfLookupFields(Integer numberOfLookupFields) {
        this.numberOfLookupFields = numberOfLookupFields;
    }

    public Boolean getDemonstrated() {
        return demonstrated;
    }

    public String getQuestiontypeId() {
        return questiontypeId;
    }

    public void setQuestiontypeId(String questiontypeId) {
        this.questiontypeId = questiontypeId;
    }

    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public String getParentQuestionId() {
        return parentQuestionId;
    }

    public void setParentQuestionId(String parentQuestionId) {
        this.parentQuestionId = parentQuestionId;
    }

    public String getChildQuestionId() {
        return childQuestionId;
    }

    public void setChildQuestionId(String childQuestionId) {
        this.childQuestionId = childQuestionId;
    }

    public String getMdCustomLookupId() {
        return mdCustomLookupId;
    }

    public void setMdCustomLookupId(String mdCustomLookupId) {
        this.mdCustomLookupId = mdCustomLookupId;
    }

    public String getMdMasterLookupId() {
        return mdMasterLookupId;
    }

    public void setMdMasterLookupId(String mdMasterLookupId) {
        this.mdMasterLookupId = mdMasterLookupId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public Questionaire text(String text) {
        this.text = text;
        return this;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Boolean isActive() {
        return active;
    }

    public Questionaire active(Boolean active) {
        this.active = active;
        return this;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Integer getPriority() {
        return priority;
    }

    public Questionaire priority(Integer priority) {
        this.priority = priority;
        return this;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public String getCategory() {
        return category;
    }

    public Questionaire category(String category) {
        this.category = category;
        return this;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getRating() {
        return rating;
    }

    public Questionaire rating(String rating) {
        this.rating = rating;
        return this;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public Boolean isDemonstrated() {
        return demonstrated;
    }

    public Questionaire demonstrated(Boolean demonstrated) {
        this.demonstrated = demonstrated;
        return this;
    }

    public void setDemonstrated(Boolean demonstrated) {
        this.demonstrated = demonstrated;
    }

    public String getSubCategory() {
        return subCategory;
    }

    public Questionaire subCategory(String subCategory) {
        this.subCategory = subCategory;
        return this;
    }

    public void setSubCategory(String subCategory) {
        this.subCategory = subCategory;
    }

    public Questiontype getQuestiontype() {
        return questiontype;
    }

    public Questionaire questiontype(Questiontype questiontype) {
        this.questiontype = questiontype;
        this.questiontypeId = questiontype != null ? questiontype.getId() : null;
        return this;
    }

    public void setQuestiontype(Questiontype questiontype) {
        this.questiontype = questiontype;
        this.questiontypeId = questiontype != null ? questiontype.getId() : null;
    }

    public Services getService() {
        return service;
    }

    public Questionaire service(Services services) {
        this.service = services;
        this.serviceId = services != null ? services.getId() : null;
        return this;
    }

    public void setService(Services services) {
        this.service = services;
        this.serviceId = services != null ? services.getId() : null;
    }

    public Questionaire getParentQuestion() {
        return parentQuestion;
    }

    public Questionaire parentQuestion(Questionaire questionaire) {
        this.parentQuestion = questionaire;
        this.parentQuestionId = questionaire != null ? questionaire.getId() : null;
        return this;
    }

    public void setParentQuestion(Questionaire questionaire) {
        this.parentQuestion = questionaire;
        this.parentQuestionId = questionaire != null ? questionaire.getId() : null;
    }

    public Integer getReferenceId() {
        return referenceId;
    }

    public void setReferenceId(Integer referenceId) {
        this.referenceId = referenceId;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public Questionaire getChildQuestion() {
        return childQuestion;
    }

    public Questionaire childQuestion(Questionaire questionaire) {
        this.childQuestion = questionaire;
        this.childQuestionId = questionaire != null ? questionaire.getId() : null;
        return this;
    }

    public void setChildQuestion(Questionaire questionaire) {
        this.childQuestion = questionaire;
        this.childQuestionId = questionaire != null ? questionaire.getId() : null;
    }

    public MdQuestionOptions getMdQuestionOptions() {
        return mdQuestionOptions;
    }

    public Questionaire mdQuestionOptions(MdQuestionOptions mdQuestionOptions) {
        this.mdQuestionOptions = mdQuestionOptions;
        this.mdCustomLookupId = mdQuestionOptions != null ? mdQuestionOptions.getId() : null;
        return this;
    }

    public void setMdQuestionOptions(MdQuestionOptions mdQuestionOptions) {
        this.mdQuestionOptions = mdQuestionOptions;
        this.mdCustomLookupId = mdQuestionOptions != null ? mdQuestionOptions.getId() : null;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Questionaire)) {
            return false;
        }
        return id != null && id.equals(((Questionaire) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Questionaire{" +
            "id=" + getId() +
            ", text='" + getText() + "'" +
            ", active='" + isActive() + "'" +
            ", priority=" + getPriority() +
            ", category='" + getCategory() + "'" +
            ", rating='" + getRating() + "'" +
            ", demonstrated='" + isDemonstrated() + "'" +
            ", subCategory='" + getSubCategory() + "'" +
            "}";
    }
}
