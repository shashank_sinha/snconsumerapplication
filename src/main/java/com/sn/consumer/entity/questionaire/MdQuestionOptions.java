package com.sn.consumer.entity.questionaire;

import com.couchbase.client.java.repository.annotation.Field;
import org.springframework.data.annotation.Id;
import org.springframework.data.couchbase.core.mapping.Document;
import org.springframework.data.couchbase.core.mapping.id.GeneratedValue;
import org.springframework.data.couchbase.core.mapping.id.IdPrefix;

import java.io.Serializable;

import static com.sn.consumer.config.Constants.ID_DELIMITER;
import static org.springframework.data.couchbase.core.mapping.id.GenerationStrategy.UNIQUE;

/**
 * A MdQuestionOptions.
 */
@Document
public class MdQuestionOptions implements Serializable {

    private static final long serialVersionUID = 1L;
    public static final String PREFIX = "mdquestionoptions";

    @SuppressWarnings("unused")
    @IdPrefix
    private String prefix = PREFIX;

    @Id
    @GeneratedValue(strategy = UNIQUE, delimiter = ID_DELIMITER)
    private String id;

    //@NotNull
    @Field("name")
    private String name;

    //@NotNull
    @Field("options")
    private String options;

    //@NotNull
    @Field("active")
    private Boolean active;

    @Field("reference_id")
    private Integer referenceId;

    @Field("language")
    private String language;

    public Boolean getActive() {
        return active;
    }

    public Integer getReferenceId() {
        return referenceId;
    }

    public void setReferenceId(Integer referenceId) {
        this.referenceId = referenceId;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public MdQuestionOptions name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOptions() {
        return options;
    }

    public MdQuestionOptions options(String options) {
        this.options = options;
        return this;
    }

    public void setOptions(String options) {
        this.options = options;
    }

    public Boolean isActive() {
        return active;
    }

    public MdQuestionOptions active(Boolean active) {
        this.active = active;
        return this;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof MdQuestionOptions)) {
            return false;
        }
        return id != null && id.equals(((MdQuestionOptions) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "MdQuestionOptions{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", options='" + getOptions() + "'" +
            ", active='" + isActive() + "'" +
            "}";
    }
}
