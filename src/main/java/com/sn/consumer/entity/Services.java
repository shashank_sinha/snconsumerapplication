package com.sn.consumer.entity;

import com.couchbase.client.java.repository.annotation.Field;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.data.annotation.Id;
import org.springframework.data.couchbase.core.mapping.Document;
import org.springframework.data.couchbase.core.mapping.id.GeneratedValue;
import org.springframework.data.couchbase.core.mapping.id.IdPrefix;
import org.springframework.data.couchbase.core.query.FetchType;
import org.springframework.data.couchbase.core.query.N1qlJoin;

import java.io.Serializable;

import static com.sn.consumer.config.Constants.ID_DELIMITER;
import static org.springframework.data.couchbase.core.mapping.id.GenerationStrategy.UNIQUE;

/**
 * A Services.
 */
@Document
public class Services extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;
    public static final String PREFIX = "services";

    @SuppressWarnings("unused")
    @IdPrefix
    private String prefix = PREFIX;

    @Id
    @GeneratedValue(strategy = UNIQUE, delimiter = ID_DELIMITER)
    private String id;

    //@NotNull
    @Field("name")
    private String name;

   // @NotNull
    @Field("level")
    private Integer level;

    @Field("priorityOrder")
    private Integer priority;

    @Field("reference_id")
    private Integer referenceId;

    @Field("language")
    private String language;

    @Field("description")
    private String description;

    @Field("advice_detail")
    private String adviceDetail;

    //@NotNull
    @Field("active")
    private Boolean active;

    @Field("parentService")
    private String parentServiceId;

    @N1qlJoin(on = "lks.parentService=meta(rks).id", fetchType = FetchType.IMMEDIATE)
    @JsonIgnoreProperties(value = "services", allowSetters = true)
    private com.sn.consumer.entity.Services parentService;

    @Field("logo_url")
    private String logoURL;

    @Field("questionnaire_description")
    private String questionnaireDescription;
    @Field("questionnaire_heading")
    private String questionnaireHeading;


    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public String getLogoURL() {
        return logoURL;
    }

    public void setLogoURL(String logoURL) {
        this.logoURL = logoURL;
    }

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public com.sn.consumer.entity.Services name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getLevel() {
        return level;
    }

    public com.sn.consumer.entity.Services level(Integer level) {
        this.level = level;
        return this;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public String getDescription() {
        return description;
    }

    public com.sn.consumer.entity.Services description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean isActive() {
        return active;
    }

    public com.sn.consumer.entity.Services active(Boolean active) {
        this.active = active;
        return this;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public com.sn.consumer.entity.Services getParentService() {
        return parentService;
    }

    public com.sn.consumer.entity.Services parentService(com.sn.consumer.entity.Services services) {
        this.parentService = services;
        this.parentServiceId = services != null ? services.getId() : null;
        return this;
    }

    public void setParentService(com.sn.consumer.entity.Services services) {
        this.parentService = services;
        this.parentServiceId = services != null ? services.getId() : null;
    }
    public Integer getReferenceId() {
        return referenceId;
    }

    public void setReferenceId(Integer referenceId) {
        this.referenceId = referenceId;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof com.sn.consumer.entity.Services)) {
            return false;
        }
        return id != null && id.equals(((com.sn.consumer.entity.Services) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    public static String getPREFIX() {
        return PREFIX;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public Boolean getActive() {
        return active;
    }

    public String getParentServiceId() {
        return parentServiceId;
    }

    public void setParentServiceId(String parentServiceId) {
        this.parentServiceId = parentServiceId;
    }

    public String getQuestionnaireDescription() {
        return questionnaireDescription;
    }

    public void setQuestionnaireDescription(String questionnaireDescription) {
        this.questionnaireDescription = questionnaireDescription;
    }

    public String getQuestionnaireHeading() {
        return questionnaireHeading;
    }

    public void setQuestionnaireHeading(String questionnaireHeading) {
        this.questionnaireHeading = questionnaireHeading;
    }

    public String getAdviceDetail() {
        return adviceDetail;
    }

    public void setAdviceDetail(String adviceDetail) {
        this.adviceDetail = adviceDetail;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Services{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", level=" + getLevel() +
            ", description='" + getDescription() + "'" +
            ", adviceDetail='" + getAdviceDetail() + "'" +
            ", active='" + isActive() + "'" +
            "}";
    }
}
