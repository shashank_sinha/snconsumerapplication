package com.sn.consumer.entity.advice;

import com.couchbase.client.java.repository.annotation.Field;
import com.sn.consumer.request.advice.AnalyticsPatternRequest;
import org.springframework.data.annotation.Id;
import org.springframework.data.couchbase.core.mapping.Document;
import org.springframework.data.couchbase.core.mapping.id.GeneratedValue;
import org.springframework.data.couchbase.core.mapping.id.IdPrefix;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

import static com.sn.consumer.config.Constants.ID_DELIMITER;
import static org.springframework.data.couchbase.core.mapping.id.GenerationStrategy.UNIQUE;

/**
 * A AnalyticsPattern.
 */
@Document
public class AnalyticsPattern implements Serializable {

    private static final long serialVersionUID = 1L;
    public static final String PREFIX = "analyticspattern";

    @SuppressWarnings("unused")
    @IdPrefix
    private String prefix = PREFIX;

    @Id
    @GeneratedValue(strategy = UNIQUE, delimiter = ID_DELIMITER)
    private String id;

    @NotNull
    @Field("pattern")
    private String pattern;

    @NotNull
    @Field("reference_id")
    private Integer referenceId;

    @NotNull
    @Field("created_by")
    private String createdBy;

    @NotNull
    @Field("language")
    private String language;

    @NotNull
    @Field("active")
    private Boolean active;

    public AnalyticsPattern(AnalyticsPatternRequest analyticsPatternRequest) {
        this.id = analyticsPatternRequest.getId();
        this.pattern = analyticsPatternRequest.getPattern();
        this.referenceId = analyticsPatternRequest.getReferenceId();
        this.createdBy = analyticsPatternRequest.getCreatedBy();
        this.language = analyticsPatternRequest.getLanguage();
        this.active = analyticsPatternRequest.isActive();
    }

    public AnalyticsPattern() {
    }

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPattern() {
        return pattern;
    }

    public AnalyticsPattern pattern(String pattern) {
        this.pattern = pattern;
        return this;
    }

    public void setPattern(String pattern) {
        this.pattern = pattern;
    }

    public Integer getReferenceId() {
        return referenceId;
    }

    public AnalyticsPattern referenceId(Integer referenceId) {
        this.referenceId = referenceId;
        return this;
    }

    public void setReferenceId(Integer referenceId) {
        this.referenceId = referenceId;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public AnalyticsPattern createdBy(String createdBy) {
        this.createdBy = createdBy;
        return this;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getLanguage() {
        return language;
    }

    public AnalyticsPattern language(String language) {
        this.language = language;
        return this;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public Boolean isActive() {
        return active;
    }

    public AnalyticsPattern active(Boolean active) {
        this.active = active;
        return this;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

}
