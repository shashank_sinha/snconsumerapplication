package com.sn.consumer.entity.advice;

import com.couchbase.client.java.repository.annotation.Field;

import com.sn.consumer.request.advice.TriggerRequest;
import org.springframework.data.annotation.Id;
import org.springframework.data.couchbase.core.mapping.id.GeneratedValue;
import org.springframework.data.couchbase.core.mapping.id.IdPrefix;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

import static com.sn.consumer.config.Constants.*;
import static org.springframework.data.couchbase.core.mapping.id.GenerationStrategy.UNIQUE;

public class Trigger implements Serializable {

    private static final long serialVersionUID = 1L;
    public static final String PREFIX = "trigger";

    @SuppressWarnings("unused")
    @IdPrefix
    private String prefix = PREFIX;

    @Id
    @GeneratedValue(strategy = UNIQUE, delimiter = ID_DELIMITER)
    private String id;

    @NotNull
    @Field("value")
    private String value;

    @NotNull
    @Field("reference_id")
    private Integer referenceId;

    @NotNull
    @Field("created_by")
    private String createdBy;

    @NotNull
    @Field("language")
    private String language;

    @NotNull
    @Field("active")
    private Boolean active;

    public Trigger(TriggerRequest triggerRequest) {
        this.id = triggerRequest.getId();
        this.value = triggerRequest.getValue();
        this.referenceId = triggerRequest.getReferenceId();
        this.createdBy = triggerRequest.getCreatedBy();
        this.language = triggerRequest.getLanguage();
        this.active = triggerRequest.isActive();
    }

    public Trigger() {
    }

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public Trigger value(String value) {
        this.value = value;
        return this;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Integer getReferenceId() {
        return referenceId;
    }

    public Trigger referenceId(Integer referenceId) {
        this.referenceId = referenceId;
        return this;
    }

    public void setReferenceId(Integer referenceId) {
        this.referenceId = referenceId;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public Trigger createdBy(String createdBy) {
        this.createdBy = createdBy;
        return this;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getLanguage() {
        return language;
    }

    public Trigger language(String language) {
        this.language = language;
        return this;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public Boolean isActive() {
        return active;
    }

    public Trigger active(Boolean active) {
        this.active = active;
        return this;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Trigger)) {
            return false;
        }
        return id != null && id.equals(((Trigger) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Trigger{" +
            "id=" + getId() +
            ", value='" + getValue() + "'" +
            ", referenceId=" + getReferenceId() +
            ", createdBy='" + getCreatedBy() + "'" +
            ", language='" + getLanguage() + "'" +
            ", active='" + isActive() + "'" +
            "}";
    }
}
