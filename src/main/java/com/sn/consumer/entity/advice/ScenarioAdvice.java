package com.sn.consumer.entity.advice;

import com.couchbase.client.java.repository.annotation.Field;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.sn.consumer.entity.Services;

import com.sn.consumer.request.advice.ScenarioAdviceRequest;
import org.springframework.data.annotation.Id;
import org.springframework.data.couchbase.core.mapping.Document;
import org.springframework.data.couchbase.core.mapping.id.GeneratedValue;
import org.springframework.data.couchbase.core.mapping.id.IdPrefix;
import org.springframework.data.couchbase.core.query.FetchType;
import org.springframework.data.couchbase.core.query.N1qlJoin;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

import static com.sn.consumer.config.Constants.ID_DELIMITER;
import static org.springframework.data.couchbase.core.mapping.id.GenerationStrategy.UNIQUE;

/**
 * A ScenarioAdvice.
 */
@Document
public class ScenarioAdvice implements Serializable {

    private static final long serialVersionUID = 1L;
    public static final String PREFIX = "scenarioadvice";

    @SuppressWarnings("unused")
    @IdPrefix
    private String prefix = PREFIX;

    @Id
    @GeneratedValue(strategy = UNIQUE, delimiter = ID_DELIMITER)
    private String id;

    @NotNull
    @Field("name")
    private String name;

    @NotNull
    @Field("description")
    private String description;

    @NotNull
    @Field("advice")
    private String advice;

    @NotNull
    @Field("variable")
    private String variable;

    @NotNull
    @Field("thumbnail_url")
    private String thumbnailUrl;

    @NotNull
    @Field("keywords")
    private String keywords;

    @Field("language")
    private String language;

    @Field("priority")
    private Integer priority;

    @NotNull
    @Field("active")
    private Boolean active;

    @NotNull
    @Field("created_by")
    private String createdBy;

    @NotNull
    @Field("reference_id")
    private Integer referenceId;

    @Field("services")
    private String servicesId;

    @N1qlJoin(on = "lks.services=meta(rks).id", fetchType = FetchType.IMMEDIATE)
    @JsonIgnoreProperties(value = "scenarioAdvices", allowSetters = true)
    private Services services;

    @Field("triggers")
    private String triggersId;

    @N1qlJoin(on = "lks.triggers=meta(rks).id", fetchType = FetchType.IMMEDIATE)
    @JsonIgnoreProperties(value = "scenarioAdvices", allowSetters = true)
    private Trigger triggers;

    public ScenarioAdvice(ScenarioAdviceRequest scenarioAdviceRequest) {
        this.id = scenarioAdviceRequest.getId();
        this.name = scenarioAdviceRequest.getName();
        this.description = scenarioAdviceRequest.getDescription();
        this.variable = scenarioAdviceRequest.getVariable();
        this.thumbnailUrl = scenarioAdviceRequest.getThumbnailUrl();
        this.keywords = scenarioAdviceRequest.getKeywords();
        this.language = scenarioAdviceRequest.getLanguage();
        this.priority = scenarioAdviceRequest.getPriority();
        this.active = scenarioAdviceRequest.isActive();
        this.createdBy = scenarioAdviceRequest.getCreatedBy();
        this.referenceId = scenarioAdviceRequest.getReferenceId();
        this.servicesId = scenarioAdviceRequest.getServicesId();
        this.triggersId = scenarioAdviceRequest.getTriggersId();
        this.advice = scenarioAdviceRequest.getAdvice();
    }

    public ScenarioAdvice() {
    }

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public ScenarioAdvice name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public ScenarioAdvice description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getVariable() {
        return variable;
    }

    public ScenarioAdvice variable(String variable) {
        this.variable = variable;
        return this;
    }

    public void setVariable(String variable) {
        this.variable = variable;
    }

    public String getThumbnailUrl() {
        return thumbnailUrl;
    }

    public ScenarioAdvice thumbnailUrl(String thumbnailUrl) {
        this.thumbnailUrl = thumbnailUrl;
        return this;
    }

    public void setThumbnailUrl(String thumbnailUrl) {
        this.thumbnailUrl = thumbnailUrl;
    }

    public String getKeywords() {
        return keywords;
    }

    public ScenarioAdvice keywords(String keywords) {
        this.keywords = keywords;
        return this;
    }

    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    public String getLanguage() {
        return language;
    }

    public ScenarioAdvice language(String language) {
        this.language = language;
        return this;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public Integer getPriority() {
        return priority;
    }

    public ScenarioAdvice priority(Integer priority) {
        this.priority = priority;
        return this;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public Boolean isActive() {
        return active;
    }

    public ScenarioAdvice active(Boolean active) {
        this.active = active;
        return this;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public ScenarioAdvice createdBy(String createdBy) {
        this.createdBy = createdBy;
        return this;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Integer getReferenceId() {
        return referenceId;
    }

    public ScenarioAdvice referenceId(Integer referenceId) {
        this.referenceId = referenceId;
        return this;
    }

    public void setReferenceId(Integer referenceId) {
        this.referenceId = referenceId;
    }

    public Services getServices() {
        return services;
    }

    public ScenarioAdvice services(Services services) {
        this.services = services;
        this.servicesId = services != null ? services.getId() : null;
        return this;
    }

    public void setServices(Services services) {
        this.services = services;
        this.servicesId = services != null ? services.getId() : null;
    }

    public Trigger getTriggers() {
        return triggers;
    }

    public ScenarioAdvice triggers(Trigger trigger) {
        this.triggers = trigger;
        this.triggersId = trigger != null ? trigger.getId() : null;
        return this;
    }

    public void setTriggers(Trigger trigger) {
        this.triggers = trigger;
        this.triggersId = trigger != null ? trigger.getId() : null;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here


    public String getServicesId() {
        return servicesId;
    }

    public void setServicesId(String servicesId) {
        this.servicesId = servicesId;
    }

    public String getTriggersId() {
        return triggersId;
    }

    public void setTriggersId(String triggersId) {
        this.triggersId = triggersId;
    }

    public String getAdvice() {
        return advice;
    }

    public void setAdvice(String advice) {
        this.advice = advice;
    }
}
