package com.sn.consumer.entity.advice;

import com.couchbase.client.java.repository.annotation.Field;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.sn.consumer.entity.Services;
import com.sn.consumer.request.advice.AnalyticsAdviceRequest;
import org.springframework.data.annotation.Id;
import org.springframework.data.couchbase.core.mapping.Document;
import org.springframework.data.couchbase.core.mapping.id.GeneratedValue;
import org.springframework.data.couchbase.core.mapping.id.IdPrefix;
import org.springframework.data.couchbase.core.query.FetchType;
import org.springframework.data.couchbase.core.query.N1qlJoin;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

import static com.sn.consumer.config.Constants.ID_DELIMITER;
import static org.springframework.data.couchbase.core.mapping.id.GenerationStrategy.UNIQUE;

/**
 * A AnalyticsAdvice.
 */
@Document
public class AnalyticsAdvice implements Serializable {

    private static final long serialVersionUID = 1L;
    public static final String PREFIX = "analyticsadvice";

    @SuppressWarnings("unused")
    @IdPrefix
    private String prefix = PREFIX;

    @Id
    @GeneratedValue(strategy = UNIQUE, delimiter = ID_DELIMITER)
    private String id;

    @NotNull
    @Field("name")
    private String name;

    @NotNull
    @Field("description")
    private String description;

    @NotNull
    @Field("variable")
    private String variable;

    @NotNull
    @Field("advice")
    private String advice;

    @NotNull
    @Field("thumbnail_url")
    private String thumbnailUrl;

    @NotNull
    @Field("language")
    private String language;

    @NotNull
    @Field("reference_id")
    private Integer referenceId;

    @NotNull
    @Field("active")
    private Boolean active;

    @NotNull
    @Field("created_by")
    private String createdBy;

    @NotNull
    @Field("priority")
    private Integer priority;

    @Field("pattern")
    private String patternId;

    @N1qlJoin(on = "lks.pattern=meta(rks).id", fetchType = FetchType.IMMEDIATE)
    private AnalyticsPattern pattern;

    @Field("services")
    private String servicesId;

    @N1qlJoin(on = "lks.services=meta(rks).id", fetchType = FetchType.IMMEDIATE)
    @JsonIgnoreProperties(value = "analyticsAdvices", allowSetters = true)
    private Services services;


    public AnalyticsAdvice(AnalyticsAdviceRequest analyticsAdviceRequest) {
        this.id = analyticsAdviceRequest.getId();
        this.name = analyticsAdviceRequest.getName();
        this.description = analyticsAdviceRequest.getDescription();
        this.variable = analyticsAdviceRequest.getVariable();
        this.advice = analyticsAdviceRequest.getAdvice();
        this.thumbnailUrl = analyticsAdviceRequest.getThumbnailUrl();
        this.language = analyticsAdviceRequest.getLanguage();
        this.referenceId = analyticsAdviceRequest.getReferenceId();
        this.active = analyticsAdviceRequest.isActive();
        this.createdBy = analyticsAdviceRequest.getCreatedBy();
        this.priority = analyticsAdviceRequest.getPriority();
        this.patternId = analyticsAdviceRequest.getPatternId();
        this.servicesId = analyticsAdviceRequest.getServicesId();
    }

    public AnalyticsAdvice() {
    }

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public AnalyticsAdvice name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public AnalyticsAdvice description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getVariable() {
        return variable;
    }

    public AnalyticsAdvice variable(String variable) {
        this.variable = variable;
        return this;
    }

    public void setVariable(String variable) {
        this.variable = variable;
    }

    public String getAdvice() {
        return advice;
    }

    public AnalyticsAdvice advice(String advice) {
        this.advice = advice;
        return this;
    }

    public void setAdvice(String advice) {
        this.advice = advice;
    }

    public String getThumbnailUrl() {
        return thumbnailUrl;
    }

    public AnalyticsAdvice thumbnailUrl(String thumbnailUrl) {
        this.thumbnailUrl = thumbnailUrl;
        return this;
    }

    public void setThumbnailUrl(String thumbnailUrl) {
        this.thumbnailUrl = thumbnailUrl;
    }

    public String getLanguage() {
        return language;
    }

    public AnalyticsAdvice language(String language) {
        this.language = language;
        return this;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public Integer getReferenceId() {
        return referenceId;
    }

    public AnalyticsAdvice referenceId(Integer referenceId) {
        this.referenceId = referenceId;
        return this;
    }

    public void setReferenceId(Integer referenceId) {
        this.referenceId = referenceId;
    }

    public Boolean isActive() {
        return active;
    }

    public AnalyticsAdvice active(Boolean active) {
        this.active = active;
        return this;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public AnalyticsAdvice createdBy(String createdBy) {
        this.createdBy = createdBy;
        return this;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Integer getPriority() {
        return priority;
    }

    public AnalyticsAdvice priority(Integer priority) {
        this.priority = priority;
        return this;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public AnalyticsPattern getPattern() {
        return pattern;
    }

    public AnalyticsAdvice pattern(AnalyticsPattern analyticsPattern) {
        this.pattern = analyticsPattern;
        this.patternId = analyticsPattern != null ? analyticsPattern.getId() : null;
        return this;
    }

    public void setPattern(AnalyticsPattern analyticsPattern) {
        this.pattern = analyticsPattern;
        this.patternId = analyticsPattern != null ? analyticsPattern.getId() : null;
    }

    public Services getServices() {
        return services;
    }

    public AnalyticsAdvice services(Services services) {
        this.services = services;
        this.servicesId = services != null ? services.getId() : null;
        return this;
    }

    public void setServices(Services services) {
        this.services = services;
        this.servicesId = services != null ? services.getId() : null;
    }

    public String getPatternId() {
        return patternId;
    }

    public void setPatternId(String patternId) {
        this.patternId = patternId;
    }

    public String getServicesId() {
        return servicesId;
    }

    public void setServicesId(String servicesId) {
        this.servicesId = servicesId;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

 }
