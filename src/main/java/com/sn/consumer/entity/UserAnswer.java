package com.sn.consumer.entity;

import com.couchbase.client.java.repository.annotation.Field;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.couchbase.core.mapping.Document;
import org.springframework.data.couchbase.core.mapping.id.GeneratedValue;
import org.springframework.data.couchbase.core.mapping.id.IdPrefix;

import java.io.Serializable;
import java.time.Instant;
import java.util.List;

import static com.sn.consumer.config.Constants.ID_DELIMITER;
import static org.springframework.data.couchbase.core.mapping.id.GenerationStrategy.UNIQUE;

@Document
public class UserAnswer implements Serializable {

    public static final String PREFIX = "User-Answer";
    private static final long serialVersionUID = 1L;
    @IdPrefix
    private String prefix = PREFIX;
    @Id
    @GeneratedValue(strategy = UNIQUE, delimiter = ID_DELIMITER)
    private String id;

    //@Not
    @Field("user_Id")
    private String userId;

    //@NotNull
    @Field("service_id")
    private String serviceId;

    @CreatedDate
    @Field("created_date")
    @JsonIgnore
    private Instant createdDate = Instant.now();

    private List<Answers> answers;

    private boolean skillAssessmentDone;

    private List<MultiAnswerAnswer> multiAnswerAnswers;

    private List<HeadingAnswer> headingAnswers;


    public List<MultiAnswerAnswer> getMultiAnswerAnswers() {
        return multiAnswerAnswers;
    }

    public void setMultiAnswerAnswers(List<MultiAnswerAnswer> multiAnswerAnswers) {
        this.multiAnswerAnswers = multiAnswerAnswers;
    }

    public List<HeadingAnswer> getMultiChoiceAnswers() {
        return headingAnswers;
    }

    public void setMultiChoiceAnswers(List<HeadingAnswer> headingAnswers) {
        this.headingAnswers = headingAnswers;
    }

    public UserAnswer(String userId, String serviceId, List<Answers> answers, boolean skillAssessmentDone) {
        this.userId = userId;
        this.serviceId = serviceId;
        this.answers = answers;
        this.skillAssessmentDone = skillAssessmentDone;
    }
    /*public UserAnswer(@NotNull String userId, @NotNull String serviceId, List<Answers> answers, boolean skillAssessmentDone) {
        this.userId = userId;
        this.serviceId = serviceId;
        this.answers = answers;
        this.skillAssessmentDone = skillAssessmentDone;
    }*/

    public UserAnswer() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public List<Answers> getAnswers() {
        return answers;
    }

    public void setAnswers(List<Answers> answers) {
        this.answers = answers;
    }

    public Instant getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
    }

    public static class Answers implements Serializable {

        @Field("question_id")
        private String questionId;

        @Field("answer")
        private String answer;

        @LastModifiedDate
        @Field("last_modified_date")
        @JsonIgnore
        private Instant lastModifiedDate = Instant.now();

        public Answers(String questionId, String answer) {
            this.questionId = questionId;
            this.answer = answer;
        }

        public Answers() {
        }

        public String getQuestionId() {
            return questionId;
        }

        public void setQuestionId(String questionId) {
            this.questionId = questionId;
        }

        public String getAnswer() {
            return answer;
        }

        public void setAnswer(String answer) {
            this.answer = answer;
        }

        public Instant getLastModifiedDate() {
            return lastModifiedDate;
        }

        public void setLastModifiedDate(Instant lastModifiedDate) {
            this.lastModifiedDate = lastModifiedDate;
        }
    }


    public boolean isSkillAssessmentDone() {
        return skillAssessmentDone;
    }

    public void setSkillAssessmentDone(boolean skillAssessmentDone) {
        this.skillAssessmentDone = skillAssessmentDone;
    }

    public static class MultiAnswerAnswer implements Serializable {

        @Field("question_id")
        private String questionId;

        @Field("multi_answers")
        private List<String> multiAnswers;

        @LastModifiedDate
        @Field("last_modified_date")
        private Instant lastModifiedDate = Instant.now();

        public MultiAnswerAnswer() {
        }

        public MultiAnswerAnswer(String questionId, List<String> multiAnswers) {
            this.questionId = questionId;
            this.multiAnswers = multiAnswers;
        }

        public Instant getLastModifiedDate() {
            return lastModifiedDate;
        }

        public void setLastModifiedDate(Instant lastModifiedDate) {
            this.lastModifiedDate = lastModifiedDate;
        }

        public String getQuestionId() {
            return questionId;
        }

        public void setQuestionId(String questionId) {
            this.questionId = questionId;
        }

        public List<String> getMultiAnswers() {
            return multiAnswers;
        }

        public void setMultiAnswers(List<String> multiAnswers) {
            this.multiAnswers = multiAnswers;
        }
    }

    public static class HeadingAnswer implements Serializable {
        @Field("question_id")
        String questionId;

        List<HeadingAnswerData> headingAnswerData;

        public static class HeadingAnswerData {
            @Field("rating")
            Integer rating;

            @Field("demo")
            boolean demo;

            @Field("heading")
            String heading;

            @LastModifiedDate
            @Field("last_modified_date")
            private Instant lastModifiedDate = Instant.now();

            public HeadingAnswerData(Integer rating, boolean demo, String heading) {
                this.rating = rating;
                this.demo = demo;
                this.heading = heading;
            }

            public HeadingAnswerData() {
            }

            public Integer getRating() {
                return rating;
            }

            public void setRating(Integer rating) {
                this.rating = rating;
            }

            public boolean isDemo() {
                return demo;
            }

            public void setDemo(boolean demo) {
                this.demo = demo;
            }

            public String getHeading() {
                return heading;
            }

            public void setHeading(String heading) {
                this.heading = heading;
            }

            public Instant getLastModifiedDate() {
                return lastModifiedDate;
            }

            public void setLastModifiedDate(Instant lastModifiedDate) {
                this.lastModifiedDate = lastModifiedDate;
            }
        }

        public List<HeadingAnswerData> getHeadingAnswerData() {
            return headingAnswerData;
        }

        public void setHeadingAnswerData(List<HeadingAnswerData> headingAnswerData) {
            this.headingAnswerData = headingAnswerData;
        }

        public String getQuestionId() {
            return questionId;
        }

        public void setQuestionId(String questionId) {
            this.questionId = questionId;
        }

        public HeadingAnswer(String questionId, List<HeadingAnswerData> headingAnswerData) {
            this.questionId = questionId;
            this.headingAnswerData = headingAnswerData;
        }

        public HeadingAnswer() {
        }
    }
}
