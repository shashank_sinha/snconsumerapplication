package com.sn.consumer.entity;

import com.couchbase.client.java.repository.annotation.Field;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


import com.sn.consumer.request.advice.AdviceRequest;
import org.springframework.data.annotation.Id;
import org.springframework.data.couchbase.core.mapping.Document;
import org.springframework.data.couchbase.core.mapping.id.GeneratedValue;
import org.springframework.data.couchbase.core.mapping.id.IdPrefix;
import org.springframework.data.couchbase.core.query.FetchType;
import org.springframework.data.couchbase.core.query.N1qlJoin;

import java.io.Serializable;

import static com.sn.consumer.config.Constants.ID_DELIMITER;
import static org.springframework.data.couchbase.core.mapping.id.GenerationStrategy.UNIQUE;

/**
 * A Advice.
 */
@Document
public class Advice implements Serializable {

    private static final long serialVersionUID = 1L;
    public static final String PREFIX = "advice";

    @SuppressWarnings("unused")
    @IdPrefix
    private String prefix = PREFIX;

    @Id
    @GeneratedValue(strategy = UNIQUE, delimiter = ID_DELIMITER)
    private String id;

    @Field("name")
    private String name;

    @Field("description")
    private String description;

    @Field("type")
    private String type;

    @Field("url")
    private String url;

    @Field("thumbnail")
    private String thumbnail;

    @Field("skillgap")
    private String keywords;

    @Field("language")
    private String language;

    @Field("reference_id")
    private Integer reference_id;

    @Field("created_by")
    private String created_by;

    @Field("priority")
    private String priority;

    @Field("job_role_category")
    private String job_role_category;

    @Field("active")
    private Boolean active;

    @Field("services")
    private String servicesId;

    @N1qlJoin(on = "lks.services=meta(rks).id", fetchType = FetchType.IMMEDIATE)
    @JsonIgnoreProperties(value = "advice", allowSetters = true)
    private Services services;


    public Advice(AdviceRequest adviceRequest) {
        this.id = adviceRequest.getId();
        this.name = adviceRequest.getName();
        this.description = adviceRequest.getDescription();
        this.type = adviceRequest.getType();
        this.url = adviceRequest.getUrl();
        this.thumbnail = adviceRequest.getThumbnail();
        this.keywords = adviceRequest.getKeywords();
        this.language = adviceRequest.getLanguage();
        this.reference_id = adviceRequest.getReference_id();
        this.created_by = adviceRequest.getCreated_by();
        this.priority = adviceRequest.getPriority();
        this.job_role_category = adviceRequest.getJob_role_category();
        this.active = adviceRequest.isActive();
        this.servicesId = adviceRequest.getServicesId();
    }

    public Advice() {
    }

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public Advice name(String name) {
        this.name = name;
        return this;
    }

    public String getServicesId() {
        return servicesId;
    }

    public void setServicesId(String servicesId) {
        this.servicesId = servicesId;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public Advice description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getType() {
        return type;
    }

    public Advice type(String type) {
        this.type = type;
        return this;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUrl() {
        return url;
    }

    public Advice url(String url) {
        this.url = url;
        return this;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public Advice thumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
        return this;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getKeywords() {
        return keywords;
    }

    public Advice keywords(String keywords) {
        this.keywords = keywords;
        return this;
    }

    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    public String getLanguage() {
        return language;
    }

    public Advice language(String language) {
        this.language = language;
        return this;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public Integer getReference_id() {
        return reference_id;
    }

    public Advice reference_id(Integer reference_id) {
        this.reference_id = reference_id;
        return this;
    }

    public void setReference_id(Integer reference_id) {
        this.reference_id = reference_id;
    }

    public String getCreated_by() {
        return created_by;
    }

    public Advice created_by(String created_by) {
        this.created_by = created_by;
        return this;
    }

    public void setCreated_by(String created_by) {
        this.created_by = created_by;
    }

    public String getPriority() {
        return priority;
    }

    public Advice priority(String priority) {
        this.priority = priority;
        return this;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public String getJob_role_category() {
        return job_role_category;
    }

    public Advice job_role_category(String job_role_category) {
        this.job_role_category = job_role_category;
        return this;
    }

    public void setJob_role_category(String job_role_category) {
        this.job_role_category = job_role_category;
    }

    public Boolean isActive() {
        return active;
    }

    public Advice active(Boolean active) {
        this.active = active;
        return this;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Services getServices() {
        return services;
    }

    public Advice services(Services services) {
        this.services = services;
        this.servicesId = services != null ? services.getId() : null;
        return this;
    }

    public void setServices(Services services) {
        this.services = services;
        this.servicesId = services != null ? services.getId() : null;
    }

    public Boolean getActive() {
        return active;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Advice)) {
            return false;
        }
        return id != null && id.equals(((Advice) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Advice{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", description='" + getDescription() + "'" +
            ", type='" + getType() + "'" +
            ", url='" + getUrl() + "'" +
            ", thumbnail='" + getThumbnail() + "'" +
            ", keywords='" + getKeywords() + "'" +
            ", language='" + getLanguage() + "'" +
            ", reference_id=" + getReference_id() +
            ", created_by='" + getCreated_by() + "'" +
            ", priority='" + getPriority() + "'" +
            ", job_role_category='" + getJob_role_category() + "'" +
            ", active='" + isActive() + "'" +
            "}";
    }
}
